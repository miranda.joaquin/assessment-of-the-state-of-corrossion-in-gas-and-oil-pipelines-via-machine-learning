# Assessment of the state of corrossion in gas and oil pipelines via machine learning


## Abstract

Based on eight variables, namely, the temperature of the wellhead, pressure of the wellhead, million standard cubic feet per day of gas, barrel of oil produced per day, the barrel of water produced per day, basic solid and water, the molecular mass of CO2, gas gravity, we compare six machine learning models (ML) to predict corrosion defects in oil and gas pipelines. The six ML models are random forest, support vector machine, k-nearest neighbor, gradient boosting, logistic regression, and decision tree. We use threshold corrosion defects to classify the state of the pipelines. Among the six models, the supportive vector machine model had the best accuracy with 95.5%, followed by the random forest classifier and K-neighbors with 90.4% and 84.1%, respectively; here we used as criteria the F1-score. In addition, we applied a reduced method analysis.
